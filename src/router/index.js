import Vue from 'vue'
import VueRouter from 'vue-router'
import home from '../components/home.vue'


//router for componenet views
Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'home',
    component: home,
    meta: {title: 'Home'}
  },
  {
    path: '/ft',
    name:'ft',
    component: () => import('../components/ft.vue'),
    meta: {title: 'Film & TV'}
  },
  {
    path: '/ttd',
    name: 'ttd',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../components/ttd.vue'),
    meta: {title: 'Things to do'}
  }, 
  {
    path: '/sa',
    name: 'sa',
    component: () => import('../components/sa.vue'),
    meta: {title: 'Societies and Activities'}
  },
  {
    path:'/uh',
    name: 'uh',
    component: () => import('../components/uh.vue'),
    meta: {title: 'University Hacks'}
  }, 

  {
    path:'/article/:id',
    name: 'articles',
    props: true,
    component: () => import('../components/article.vue')  ,
    meta: {title: 'Article'}

  }

]
//history mode to remove hash
const router = new VueRouter({
  mode: 'history',
  routes
})
//titles for pages added
router.beforeEach((to, from, next) => {
  document.title = to.meta.title
  next()
})
export default router
